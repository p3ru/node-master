var app = require('express')();
var http = require('http').Server(app);
var WebSocket = require('ws');
var wss = new WebSocket.Server({ port: 8080 });

wss.on('connection', function connection(ws) {
  console.log("Connection!");
  ws.on('message', function incoming(message) {
    console.log('received: %s', message);
    ws.send('Oh hi there!');
  });

  ws.send('something');
});

http.listen(8081, function () {
  console.log('listening on *:8081');
});